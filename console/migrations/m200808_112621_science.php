<?php

use yii\db\Migration;

/**
 * Class m200808_112621_science
 */
class m200808_112621_science extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%science}}', [
            'id' => $this->primaryKey(),
            'creator_id' => $this->integer()->notNull(),
            'science_name' => $this->string()->notNull()->unique(),
            'number_of_questions' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(\common\models\constants\CommonStatus::STATUS_NON_ACTIVE),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->integer(),
        ], $tableOptions);

        $this->insert('{{%science}}', [
            'id' => 1,
            'creator_id' => 1,
            'science_name' => 'Informatika',
            'number_of_questions' => 10,
            'status' => \common\models\constants\CommonStatus::STATUS_ACTIVE,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
