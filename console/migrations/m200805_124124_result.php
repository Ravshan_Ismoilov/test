<?php

use yii\db\Migration;

/**
 * Class m200805_124124_result
 */
class m200805_124124_result extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%result}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'full_name' => $this->string()->notNull(),
            'question_id' => $this->integer()->notNull(),
            'right_answer' => $this->integer()->notNull(),
            'user_answer' => $this->integer(),
            'status' => $this->integer()->notNull()->defaultValue(\common\models\constants\CommonStatus::STATUS_NON_ACTIVE),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%result}}');
    }
}