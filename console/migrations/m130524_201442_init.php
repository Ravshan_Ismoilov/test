<?php

use common\models\constants\UserRole;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'creator_id' => $this->integer()->notNull(),
            'phone' => $this->string()->notNull()->unique(),
            'full_name' => $this->string()->null(),
            'image' => $this->string()->notNull()->defaultValue('/nophoto.png'),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'role' => $this->integer()->notNull()->defaultValue(\common\models\constants\UserRole::ROLE_USER),
            'detail_id' => $this->integer()->null(),
            'verification_token' => $this->string()->null(),
            'status' => $this->integer()->notNull()->defaultValue(\common\models\constants\CommonStatus::STATUS_NON_ACTIVE),
            'last_seen' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->insert('{{%user}}', [
            'id' => 1,
            'creator_id' => 1,
            'phone' => '+998939461188',
            'full_name' => 'Ravshan Ismoilov',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('0'),
            'password_reset_token' => Yii::$app->security->generateRandomString(),
            'email' => 'ravshan.ismoilov77@gmail.com',
            'role' => UserRole::ROLE_ADMIN,
            'status' => \common\models\constants\CommonStatus::STATUS_ACTIVE,
            'last_seen' => time(),
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%log}}');
    }
}
