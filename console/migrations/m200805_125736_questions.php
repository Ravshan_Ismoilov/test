<?php

use yii\db\Migration;

/**
 * Class m200805_125736_questions
 */
class m200805_125736_questions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%questions}}', [
            'id' => $this->primaryKey(),
            'creator_id' => $this->integer()->notNull(),
            'science' => $this->string()->notNull(),
            'question' => $this->string()->notNull(),
            'answer_one' => $this->string()->notNull(),
            'answer_two' => $this->string()->notNull(),
            'answer_three' => $this->string()->notNull(),
            'answer_four' => $this->string()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(\common\models\constants\CommonStatus::STATUS_ACTIVE),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->integer(),
            
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%questions}}');
    }
}