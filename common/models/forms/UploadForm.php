<?php


namespace common\models\forms;

use common\models\constants\UserDetailFields;
use common\models\User;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for "UserDetailFields".
 *
 * @property UploadedFile $file
 */
class UploadForm extends Model
{
    public $file;

    public $label;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => 'pdf, jpg, png, jpeg', 'maxSize' => 5 * 1024 * 1024],
            [['file'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Fayl tanlang'
        ];
    }

    /**
     * @param null $fileName
     * @return string|null
     */
    public function uploader($fileName = null)
    {
        $this->file = UploadedFile::getInstance($this, 'file');
        if ($fileName == null)
            $fileName = $this->file->baseName . time();
        $fileName .= '.' . $this->file->extension;
        $inputFile = \Yii::getAlias('@assets/user/uploads/') . $fileName;
        if ($this->file && $this->validate()) {
            $this->file->saveAs($inputFile);
            return $fileName;
        }
        return null;
    }

    /**
     * @param User $user
     * @param $attribute
     * @return bool
     */
    public function upload(User $user, $attribute)
    {
        $file_name = $this->uploader(UserDetailFields::getLabel($attribute).'_'.$user->full_name);
        $details = $user->details;
        $details->{$attribute} = $file_name;
        if($details->save())
            return true;
        return false;
    }
}