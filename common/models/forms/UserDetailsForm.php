<?php

namespace common\models\forms;

use common\models\UserDetails;
use http\Exception;
use Yii;
use yii\base\DynamicModel;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user_details".
 *
 * @property int $id
 * @property int $creator_id
 * @property string $application
 * @property string|null $recruitment_order
 * @property string|null $employment_contract
 * @property string $passport
 * @property string $diploma
 * @property string $workbook
 * @property string $medical_check
 * @property string $reference
 * @property string $survey
 * @property string $biography
 * @property string $small_pictures
 * @property string $pension
 * @property string $inn
 * @property string $t2_card
 * @property string $non_conviction
 * @property int $position
 * @property string $description
 * @property string $certification
 * @property string $training_certification
 * @property string $dismissal
 * @property string|null $dismissal_order
 *
 *
 * @property UserDetails $_user_details
 */
class UserDetailsForm extends Model
{

    public $id;
    public $creator_id;
    public $application;
    public $recruitment_order;
    public $employment_contract;
    public $passport;
    public $diploma;
    public $workbook;
    public $medical_check;
    public $reference;
    public $survey;
    public $biography;
    public $small_pictures;
    public $pension;
    public $inn;
    public $t2_card;
    public $non_conviction;
    public $position;
    public $description;
    public $certification;
    public $training_certification;
    public $dismissal;
    public $dismissal_order;

    private $_user_details;

    /**
     * UserDetailsForm constructor.
     * @param UserDetails|null $model
     * @param array $config
     */
    public function __construct(UserDetails $model = null, $config = [])
    {
        if ($model != null) {
            $this->_user_details = $model;
            $this->id = $model->id;
            $this->creator_id = $model->creator_id;
            $this->application = $model->application;
            $this->recruitment_order = $model->recruitment_order;
            $this->employment_contract = $model->employment_contract;
            $this->passport = $model->passport;
            $this->diploma = $model->diploma;
            $this->workbook = $model->workbook;
            $this->medical_check = $model->medical_check;
            $this->reference = $model->reference;
            $this->survey = $model->survey;
            $this->biography = $model->biography;
            $this->small_pictures = $model->small_pictures;
            $this->pension = $model->pension;
            $this->inn = $model->inn;
            $this->t2_card = $model->t2_card;
            $this->non_conviction = $model->non_conviction;
            $this->position = $model->position;
            $this->description = $model->description;
            $this->certification = $model->certification;
            $this->training_certification = $model->training_certification;
            $this->dismissal = $model->dismissal;
            $this->dismissal_order = $model->dismissal_order;

        } else {
            $this->_user_details = new UserDetails();
        }
        parent::__construct($config);
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['creator_id', 'application', 'passport', 'diploma', 'workbook', 'medical_check', 'reference', 'survey', 'biography', 'small_pictures', 'pension', 'inn', 't2_card', 'non_conviction', 'position', 'description', 'certification', 'training_certification', 'dismissal'], 'required'],
            [['creator_id', 'position', 'status', 'created_at', 'updated_at', 'deleted_at'], 'default', 'value' => null],
            [['creator_id', 'position', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['application'], 'file']
//            [['application', 'recruitment_order', 'employment_contract', 'passport', 'diploma', 'workbook', 'medical_check', 'reference', 'survey', 'biography', 'small_pictures', 'pension', 'inn', 't2_card', 'non_conviction', 'description', 'certification', 'training_certification', 'dismissal', 'dismissal_order'], 'file', 'extensions' => 'jpg, png, pdf'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'creator_id' => Yii::t('yii', 'Creator ID'),
            'application' => Yii::t('yii', 'Ариза (.pdf)'),
            'recruitment_order' => Yii::t('yii', 'Ишга қабул қилиш тўғрисидаги буйруқ (.pdf)'),
            'employment_contract' => Yii::t('yii', 'Меҳнат шартномаси (.pdf)'),
            'passport' => Yii::t('yii', 'Паспорт нусхаси (.pdf)'),
            'diploma' => Yii::t('yii', 'Диплом ёки аттестат (нусхаси) (.pdf)'),
            'workbook' => Yii::t('yii', 'Меҳнат дафтарчаси (асли) (.pdf)'),
            'medical_check' => Yii::t('yii', 'Тиббий дафтарча (асли) (.pdf) '),
            'reference' => Yii::t('yii', 'Маълумотнома (обьективка, расм билан) (.doc yoki .pdf)'),
            'survey' => Yii::t('yii', 'Сўровнома (.pdf)'),
            'biography' => Yii::t('yii', 'Таржимаи хол (.pdf)'),
            'small_pictures' => Yii::t('yii', 'Расм 3х4 хажмдаги, орқа фони оқ, расмий кийимда (.jpg yoki .png)'),
            'pension' => Yii::t('yii', 'Пенция жамғармаси дафтарчаси (нусхаси) (.pdf)'),
            'inn' => Yii::t('yii', 'Солиқ ИНН рақами (.pdf)'),
            't2_card' => Yii::t('yii', 'Т-2 карточкаси (.pdf)'),
            'non_conviction' => Yii::t('yii', 'Судланмаганлик ҳақида маълумот (ДХМ дан) (.pdf)'),
            'position' => Yii::t('yii', 'Лавозим-йўриқномаси (Allow)'),
            'description' => Yii::t('yii', 'Тавсифнома (.pdf)'),
            'certification' => Yii::t('yii', 'Аттестация варақаси (.pdf)'),
            'training_certification' => Yii::t('yii', 'Малака оширганлиги тўғрисидаги сертификат нусхаси (.pdf)'),
            'dismissal' => Yii::t('yii', 'Ишдан бўшаш тўғрисидаги ариза (.pdf)'),
            'dismissal_order' => Yii::t('yii', 'Ишдан бўшатиш тўғрисидаги буйруқ (.pdf)'),
            'status' => Yii::t('yii', 'Holati'),
            'created_at' => Yii::t('yii', 'Yaratilgan vaqti'),
            'updated_at' => Yii::t('yii', 'O`zgartirilgan vaqti'),
            'deleted_at' => Yii::t('yii', 'O`chirilgan vaqti'),
        ];
    }

    public function save()
    {

    }
}