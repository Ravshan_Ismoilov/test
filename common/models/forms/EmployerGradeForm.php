<?php

namespace common\models\forms;

use common\helpers\Utilities;
use common\models\constants\CommonStatus;
use common\models\EmployeeGrade;
use common\models\Employeer;
use Yii;
use yii\base\Model;


/**
 * This is the model class for table "employeer".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @property array $grades
 * @property Employeer $_employeer
 */
class EmployerGradeForm extends Model
{

    public $id;
    public $name;
    public $description;
    public $status;

    private $_employeer;

    public $grades;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'grades', 'description'], 'required'],
            [['name', 'description'], 'string'],
            [['status', 'id'], 'integer'],
            [['grades'], 'safe'],
        ];
    }

    /**
     * EmployerGradeForm constructor.
     * @param Employeer|null $employeer
     * @param array $config
     */
    public function __construct(Employeer $employeer = null, $config = [])
    {
        if($employeer != null){
            $this->_employeer = $employeer;
            $this->id = $employeer->id;
            $this->name = $employeer->name;
            $this->description = $employeer->description;
            $this->status = $employeer->status;

            $this->grades = [];

            $grades = $employeer->grades;

            /** @var EmployeeGrade $grade */
            foreach ($grades as $grade){
                $this->grades[] = [
                    'id' => $grade->id,
                    'name' => $grade->name,
                    'rank' => $grade->rank,
                    'amount' => $grade->amount
                ];
            }
        } else {
            $this->_employeer = new Employeer();
            $this->description = "ularning tur xil toifalari mavjud";
            $this->grades = [];
        }
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'creator_id' => Yii::t('yii', 'Tizimgan qo`shgan odam'),
            'name' => Yii::t('yii', 'Nomi'),
            'description' => Yii::t('yii', 'Xususiyati'),
            'status' => Yii::t('yii', 'Holati'),
            'created_at' => Yii::t('yii', 'Yaratilgan vaqti'),
            'updated_at' => Yii::t('yii', 'O`zgartirilgan vaqti'),
            'deleted_at' => Yii::t('yii', 'O`chirilgan vaqti'),
            'grades' => Yii::t('yii', 'Razryadlar'),
        ];
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save()
    {
        if(!is_array($this->grades))
            $this->grades = [];

        $this->_employeer->name = $this->name;
        $this->_employeer->description = $this->description;
        if(!$this->id){
            $this->_employeer->creator_id = Yii::$app->user->identity->id;
        }
        $this->_employeer->status = CommonStatus::STATUS_ACTIVE;
        if(is_int($this->status)){
            $this->_employeer->status = $this->status;
        }
        if(!$this->_employeer->save()) {
            return false;
        }
        $this->id = $this->_employeer->id;
        $ids = [];
        foreach ($this->grades as $grade){
            if ($grade['id'] != "-1")
                $ids[] = $grade['id'];
        }
        $models = EmployeeGrade::findActive()->andWhere(['and', ['not', ['in', 'id', $ids]], ['employee_id' => $this->id]])->all();
        foreach ($models as $model)
            $model->delete();
        foreach ($this->grades as $grade){
            $new_grade = new EmployeeGrade();
            $new_grade->creator_id = \Yii::$app->user->identity->id;
            $new_grade->employee_id = $this->id;
            $new_grade->name = $grade['name'];
            $new_grade->rank = intval($grade['rank']);
            $new_grade->amount = doubleval($grade['amount']);
            $new_grade->status = CommonStatus::STATUS_ACTIVE;
            if(!$new_grade->save()){
                throw new \DomainException("Razryad qo`shishda xatolik: ". Utilities::varDumpToString($new_grade->errors));
            }
        }
        return true;
    }
}