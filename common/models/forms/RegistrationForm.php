<?php

namespace common\models\forms;

use borales\extensions\phoneInput\PhoneInputValidator;
use common\models\constants\CommonStatus;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegistrationForm extends Model
{
    public $phone;
    public $email;
    public $password;
    public $confirm_password;
    public $captcha;
    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['phone', 'email', 'captcha', 'password', 'confirm_password'], 'required'],
            [['password', 'confirm_password'], 'string'],
            [['email'], 'email'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароллар бир-бирига мос келмайди'],
            ['captcha', 'captcha', 'captchaAction' => 'site/captcha'],
            [['phone'], PhoneInputValidator::className()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'phone' => Yii::t('yii', 'Telefon raqam'),
            'email' => Yii::t('yii', 'Elektron pochta'),
            'password' => Yii::t('yii', 'Parol'),
            'confirm_password' => Yii::t('yii', 'Parolni tasdiqlash'),
            'captcha' => Yii::t('yii', 'Kalit so`z'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function register()
    {
        $user = new User();
        $user->status = CommonStatus::STATUS_ACTIVE;
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        $user->generateEmailVerificationToken();
        $user->created_at = time();
        $user->updated_at = time();
        $user->last_seen = time();
        if ($user->save())
            return $this->sentEmailConfirm();
        return false;
    }

    /**
     * @param User $user
     */
    public function sentEmailConfirm(User $user)
    {
        $email = $user->email;

        $sent = Yii::$app->mailer
            ->compose(
                ['html' => 'user-signup-comfirm-html', 'text' => 'user-signup-comfirm-text'],
                ['user' => $user])
            ->setTo($email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Confirmation of registration')
            ->send();

        if (!$sent) {
            throw new \RuntimeException('Sending error.');
        }
    }


    /**
     * @param $token
     */
    public function confirmation($token)
    {
        if (empty($token)) {
            throw new \DomainException('Empty confirm token.');
        }

        $user = User::findOne(['verification_token' => $token]);
        if (!$user) {
            throw new \DomainException('User is not found.');
        }

        $user->verification_token = null;
        $user->status = CommonStatus::STATUS_ACTIVE;
        if (!$user->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->_user = $user;
        if (!$this->login()) {
            throw new \RuntimeException('Error authentication.');
        }
        return true;
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByPhone($this->phone);
        }

        return $this->_user;
    }
}
