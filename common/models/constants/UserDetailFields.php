<?php

namespace common\models\constants;


use common\models\UserDetails;

/**
 * This is the model class for "UserDetailFields".
 *
 * @property array $fields
 * @property string $field
 */
class UserDetailFields
{
    private $fields = [
        'application',
        'recruitment_order',
        'employment_contract',
        'passport',
        'diploma',
        'workbook',
        'medical_check',
        'reference',
        'survey',
        'biography',
        'small_pictures',
        'pension',
        'inn',
        't2_card',
        'non_conviction',
        'description',
        'certification',
        'training_certification',
        'dismissal',
        'dismissal_order'
    ];

    /**
     * @return array
     */
    public static function getFields()
    {
        return [
            'application',
            'recruitment_order',
            'employment_contract',
            'passport',
            'diploma',
            'workbook',
            'medical_check',
            'reference',
            'survey',
            'biography',
            'small_pictures',
            'pension',
            'inn',
            't2_card',
            'non_conviction',
            'description',
            'certification',
            'training_certification',
            'dismissal',
            'dismissal_order'
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getField($id)
    {
        return self::getFields()[$id];
    }

    /**
     * @param $attr
     * @return string
     */
    public static function getLabel($attr)
    {
        return (new UserDetails())->getAttributeLabel($attr);
    }
}