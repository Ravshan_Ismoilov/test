<?php


namespace common\models\constants;

use Yii;

class UserRole
{
    const ROLE_USER = 0;
    const ROLE_ACCOUNTANT = 1;
    const ROLE_STAFF = 2;
    const ROLE_RECTOR = 5;
    const ROLE_ADMIN = 10;

    /**
     * @param $user_role
     * @return string
     */
    public static function getString($user_role)
    {
        switch ($user_role){
            case self::ROLE_USER:
                return Yii::t('yii', 'Xodim');
            case self::ROLE_ACCOUNTANT:
                return Yii::t('yii', 'Moliyachi');
            case self::ROLE_STAFF:
                return Yii::t('yii', 'Kadr');
            case self::ROLE_RECTOR:
                return Yii::t('yii', 'Rektor');
            case self::ROLE_ADMIN:
                return Yii::t('yii', 'Admin');
            default:
                return Yii::t('yii', 'Неизвестно');
        }
    }

    /**
     * @return array
     */
    public static function getArray()
    {
        $roles = [
            self::ROLE_USER => self::getString(self::ROLE_USER),
            self::ROLE_ACCOUNTANT => self::getString(self::ROLE_ACCOUNTANT),
            self::ROLE_STAFF => self::getString(self::ROLE_STAFF),
            self::ROLE_RECTOR => self::getString(self::ROLE_RECTOR),
            self::ROLE_ADMIN => self::getString(self::ROLE_ADMIN)
        ];
        $result = [];
        foreach ($roles as $key => $val)
            if($key <= Yii::$app->user->identity->role)
                $result[$key] = $val;
        return $result;
    }
}
