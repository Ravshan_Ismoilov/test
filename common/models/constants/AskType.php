<?php


namespace common\models\constants;


class AskType
{
    const ASK_INSERT = 1;
    const ASK_UPDATE = 2;
    const ASK_DELETE = 3;

    /**
     * @param $status
     * @return string
     */
    public static function getString($status)
    {
        switch ($status) {
            case self::ASK_INSERT:
                return \Yii::t('yii', 'Qo`shish so`rovi');
            case self::ASK_UPDATE:
                return \Yii::t('yii', 'O`zgartirish so`rovi');
            case self::ASK_DELETE:
                return \Yii::t('yii', 'O`chirish so`rovi');
        }
        return \Yii::t('yii', 'Noaniq so`rov');
    }

    /**
     * @return array
     */
    public static function getArray()
    {
        return [
            self::ASK_INSERT => self::getString(self::ASK_INSERT),
            self::ASK_UPDATE => self::getString(self::ASK_UPDATE),
            self::ASK_DELETE => self::getString(self::ASK_DELETE),
        ];
    }

}
