<?php


namespace common\models\constants;


class CommonStatus
{
    const STATUS_DELETED    = -1;
    const STATUS_NON_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    /**
     * @param $status
     * @return string
     */
    public static function getString($status)
    {
        switch ($status) {
            case self::STATUS_DELETED:
                return \Yii::t('yii', 'O`chirilgan');
            case self::STATUS_NON_ACTIVE:
                return \Yii::t('yii', 'Faol emas');
            case self::STATUS_ACTIVE:
                return \Yii::t('yii', 'Faol');
        }
        return \Yii::t('yii', 'Aniq emas');
    }

    /**
     * @return array
     */
    public static function getArray()
    {
        return (\Yii::$app->user->identity->role == UserRole::ROLE_ADMIN ? [
            self::STATUS_ACTIVE => self::getString(self::STATUS_ACTIVE),
            self::STATUS_NON_ACTIVE => self::getString(self::STATUS_NON_ACTIVE),
            self::STATUS_DELETED => self::getString(self::STATUS_DELETED),
        ] : [
            self::STATUS_ACTIVE => self::getString(self::STATUS_ACTIVE),
            self::STATUS_NON_ACTIVE => self::getString(self::STATUS_NON_ACTIVE),
        ]);
    }

    /**
     * @param $status
     * @return string
     */
    public static function getColor($status)
    {
        switch ($status) {
            case self::STATUS_DELETED:
                return 'gray';
            case self::STATUS_NON_ACTIVE:
                return 'yellow;';
            case self::STATUS_ACTIVE:
                return '#83FF33';
        }
        return 'white';
    }

    /**
     * @param $status
     * @return string
     */
    public static function getRowColor($x)
    {
        if($x > 0)
            return '#ff9999';
        return null;
    }

}
