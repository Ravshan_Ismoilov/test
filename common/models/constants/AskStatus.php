<?php


namespace common\models\constants;


class AskStatus
{
    const STATUS_DELETED = -1;
    const STATUS_WAITING = 0;
    const STATUS_REJECT = 1;
    const STATUS_ACCEPT = 2;

    /**
     * @param $status
     * @return string
     */
    public static function getString($status)
    {
        switch ($status) {
            case self::STATUS_DELETED:
                return \Yii::t('yii', 'O`chirilgan');
            case self::STATUS_WAITING:
                return \Yii::t('yii', 'Kutilmoqda');
            case self::STATUS_REJECT:
                return \Yii::t('yii', 'Bekor qilindi');
            case self::STATUS_ACCEPT:
                return \Yii::t('yii', 'Ruxsat berildi');
        }
        return \Yii::t('yii', 'Aniq emas');
    }

    /**
     * @return array
     */
    public static function getArray()
    {
        return (\Yii::$app->user->identity->role == UserRole::ROLE_ADMIN ? [
            self::STATUS_DELETED => self::getString(self::STATUS_DELETED),
            self::STATUS_WAITING => self::getString(self::STATUS_WAITING),
            self::STATUS_REJECT => self::getString(self::STATUS_REJECT),
            self::STATUS_ACCEPT => self::getString(self::STATUS_ACCEPT),
        ] : [
            self::STATUS_WAITING => self::getString(self::STATUS_WAITING),
            self::STATUS_REJECT => self::getString(self::STATUS_REJECT),
            self::STATUS_ACCEPT => self::getString(self::STATUS_ACCEPT),
        ]);
    }

    /**
     * @param $status
     * @return string
     */
    public static function getColor($status)
    {
        switch ($status) {
            case self::STATUS_DELETED:
                return 'gray';
            case self::STATUS_WAITING:
                return 'yellow;';
            case self::STATUS_ACCEPT:
                return '#83FF33';
            case self::STATUS_REJECT:
                return '#FF9333';
        }
        return 'white';
    }

}
