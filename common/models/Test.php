<?php

namespace common\models;

use Yii;

class Test extends BaseTimestampedModel
{
    /**
     * {@inheritdoc}
     */
    public $science;
    public $level;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['science', 'level'], 'required'],
            ['science', 'level', 'message' => 'Bo\'limni tanlang...'],
            [['level'], 'integer'],
            [['science'], 'string', 'max' => 255],
        ];
    }
}
