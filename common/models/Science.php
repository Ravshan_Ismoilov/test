<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "science".
 *
 * @property int $id
 * @property int $creator_id
 * @property string $science_name
 * @property int $number_of_questions
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 */
class Science extends BaseTimestampedModel
{
    public $level;
    public $science;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'science';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['science_name', 'number_of_questions'], 'required'],
            [['creator_id', 'number_of_questions', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['science_name'], 'string', 'max' => 255],
            [['science_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'creator_id' => Yii::t('app', 'Creator ID'),
            'science_name' => Yii::t('app', 'Science Name'),
            'number_of_questions' => Yii::t('app', 'Number Of Questions'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    public function sciencecreate()
    {
        $science = new Science();
        $science->creator_id = Yii::$app->user->id;
        $science->status = $this->status;
        $science->science_name = $this->science_name;
        $science->number_of_questions = $this->number_of_questions;
        $science->created_at = time();
        $science->updated_at = time();
        if ($science->save())
            return true;
        return false;
    }
}
