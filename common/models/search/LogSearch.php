<?php

namespace common\models\search;

use common\helpers\Utilities;
use common\models\constants\UserRole;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Log;

/**
 * LogSearch represents the model behind the search form of `common\models\Log`.
 */
class LogSearch extends Log
{
    public $begin;
    public $end;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_at'], 'integer'],
            [['text', 'begin', 'end'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Log::find();
        if(\Yii::$app->user->identity->role < UserRole::ROLE_ADMIN)
            $query->where(['>', 'user_id', 1]);
        if(\Yii::$app->user->identity->role < UserRole::ROLE_RECTOR)
            $query->andWhere(['user_id' => -1]);
        $query->orderBy(['id' => SORT_DESC]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if ($this->begin !== "" && !is_null($this->begin)) {
            $start_date = Utilities::toUnixDate($this->begin);
            $end_date = max(Utilities::toUnixDate($this->end),  $start_date + 60 * 60 * 24);
            $query->andFilterWhere(['between', 'created_at', $start_date, $end_date]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['ilike', 'text', $this->text]);

        return $dataProvider;
    }
}
