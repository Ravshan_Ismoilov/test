<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "result".
 *
 * @property int $id
 * @property int $user_id
 * @property string $full_name
 * @property int $question_id
 * @property int $right_answer
 * @property int|null $user_answer
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Result extends BaseTimestampedModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'full_name', 'question_id', 'right_answer'], 'required'],
            [['user_id', 'question_id', 'right_answer', 'user_answer', 'status', 'created_at', 'updated_at'], 'integer'],
            [['full_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'full_name' => Yii::t('app', 'Full Name'),
            'question_id' => Yii::t('app', 'Question ID'),
            'right_answer' => Yii::t('app', 'Right Answer'),
            'user_answer' => Yii::t('app', 'User Answer'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
