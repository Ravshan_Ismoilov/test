<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property int $creator_id
 * @property string $science
 * @property string $question
 * @property string $answer_one
 * @property string $answer_two
 * @property string $answer_three
 * @property string $answer_four
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 */
class Questions extends BaseTimestampedModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['creator_id', 'science', 'question', 'answer_one', 'answer_two', 'answer_three', 'answer_four'], 'required'],
            [['creator_id', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['science', 'question', 'answer_one', 'answer_two', 'answer_three', 'answer_four'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'creator_id' => Yii::t('app', 'Creator ID'),
            'science' => Yii::t('app', 'Science'),
            'question' => Yii::t('app', 'Question'),
            'answer_one' => Yii::t('app', 'Answer One'),
            'answer_two' => Yii::t('app', 'Answer Two'),
            'answer_three' => Yii::t('app', 'Answer Three'),
            'answer_four' => Yii::t('app', 'Answer Four'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }
}
