<?php

namespace common\helpers;

use common\models\constants\UserRole;
use kartik\file\FileInput;
use Yii;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;

class Utilities
{



    /**
     * @param $query
     * @return Pagination
     */
    public static function paginationUtil($query, $page_size = 6)
    {
        return new Pagination([
            'defaultPageSize' => $page_size,
            'totalCount' => $query->count(),
            'pageParam' => 'page',
        ]);
    }
    /**
     * @param $array
     * @return string
     */
    public static function addParamToUrl($array){
        $params = $_GET;
        unset($params['language']);
        return Url::to(array_merge([Yii::$app->request->pathInfo], array_merge($params, $array)));
    }

    /**
     * @param array $items
     * @return array
     */
    public static function getActionColumn($items = [1, 1, 1], $table = null)
    {
        return [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
            'header' => Yii::t('yii', ''),
            'buttons' => [
                'view' => function ($url, $model) use ($items) {
                    if (!$items[0]) return '';
                    return Html::a('<span class="fa fa-eye"></span>', $url, [
                        'title' => Yii::t('yii', 'View'),
                        'class' => 'btn btn-info',
                        'data-pjax' => 0,
                    ]);
                },
                'update' => function ($url, $model) use ($items) {
                    if (!$items[1]) return '';
                    return Html::a('<span class="fa fa-pencil"></span>', $url, [
                        'title' => Yii::t('yii', 'Update'),
                        'class' => 'btn btn-warning',
                        'data-pjax' => 0,
                    ]);
                },
                'delete' => function ($url, $model) use ($table, $items) {
                    if (!$items[2]) return '';
                    return Yii::$app->user->identity->role >= UserRole::ROLE_RECTOR ? Html::a('<span class="fa fa-trash"></span>', $url, [
                        'title' => Yii::t('yii', 'Delete'),
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                            'pjax' => 0,
                        ],
                    ]) :
                        Html::a('<span class="fa fa-trash"></span>', Url::to(['/ask-todo/execute', 'table' => $table, 'id' => $model->id]), [
                            'title' => Yii::t('yii', 'Delete'),
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                                'pjax' => 0,
                            ],
                        ]) ;
                },
            ],
        ];
    }


    /**
     * @param $param
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function toStringDate($param)
    {
        if ($param == null)
            $param = (new \DateTime())->sub(\DateInterval::createFromDateString("1 days"));
        return Yii::$app->formatter->asDate($param);
    }

    /**
     * @param $param
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function toStringDateTime($param)
    {
        if ($param == null)
            $param = (new \DateTime())->sub(\DateInterval::createFromDateString("1 days"));
        return Yii::$app->formatter->asDatetime($param);
    }

    /**
     * @param $param
     * @return int
     * @throws \Exception
     */
    public static function toUnixDate($param)
    {
        $date = new \DateTime($param);
        return $date->getTimestamp();
    }

    /**
     * @param $var
     * @return false|string
     */
    public static function varDumpToString($var)
    {
        ob_start();
        var_dump($var);
        $result = ob_get_clean();
        return $result;
    }

    /**
     * @param $json
     * @return string
     */
    public static function prettyPrint($json)
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = NULL;
        $json_length = strlen($json);

        for ($i = 0; $i < $json_length; $i++) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if ($ends_line_level !== NULL) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if ($in_escape) {
                $in_escape = false;
            } else if ($char === '"') {
                $in_quotes = !$in_quotes;
            } else if (!$in_quotes) {
                switch ($char) {
                    case '}':
                    case ']':
                        $level--;
                        $ends_line_level = NULL;
                        $new_line_level = $level;
                        break;

                    case '{':
                    case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ":
                    case "\t":
                    case "\n":
                    case "\r":
                        $char = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level = NULL;
                        break;
                }
            } else if ($char === '\\') {
                $in_escape = true;
            }
            if ($new_line_level !== NULL) {
                $result .= "\n" . str_repeat("\t", $new_line_level);
            }
            $result .= $char . $post;
        }

        return $result;
    }

    /**
     * @param $before
     * @param $now
     * @return string
     */
    public static function timeAgo($before, $now)
    {
        $intervals = array(
            1 => Yii::t('yii', 'second'),
            60 => Yii::t('yii', 'minute'),
            60 * 60 => Yii::t('yii', 'hour'),
            60 * 60 * 24 => Yii::t('yii', 'day'),
            60 * 60 * 24 * 7 => Yii::t('yii', 'week'),
            60 * 60 * 24 * 30 => Yii::t('yii', 'month'),
            60 * 60 * 24 * 365 => Yii::t('yii', 'year'),
            60 * 60 * 24 * 365 * 100 => Yii::t('yii', 'Asr'),
        );

        $dif = $now - $before;
        if ($dif < 10)
            return Yii::t('yii', '~ recently');
        $prekey = 1;
        $preval = '';
        foreach ($intervals as $key => $val) {
            $time = intval($dif / $prekey);
            if (intval($dif / $key) < 1)
                return Yii::t('yii', '~ {time} {key} ago', [
                    'time' => ceil($time),
                    'key' => $preval
                ]);
            $prekey = $key;
            $preval = $val;
        }
    }

    /**
     * @param $time
     * @return false|string
     */
    public static function dateFormat($time)
    {
        return date('d.m.Y H:i:s', $time);
    }

    /**
     * @param $value
     * @param int $del
     * @return string|string[]
     */
    public static function printFormatter($value, $del = 2)
    {
        return str_replace(",", ".", number_format($value, $del));
    }

    /**
     * @param $file
     * @return string
     * @throws \Exception
     */
    public static function makeEmbed($file)
    {
        if ($file != null) {
            $ext = substr($file, -3);
            return FileInput::widget([
                'name' => 'dfsf_sdfs',
                'pluginOptions' => [
                    'initialPreview' => [
                        Yii::getAlias('@assets_url/user/uploads/') . $file,
                    ],
                    'previewFileType' => 'any',
                    'initialPreviewConfig' => [
                        $ext == "pdf" ? ['caption' => $file, 'type' => $ext] : [],
                    ],
                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'browseLabel' => '',
                    'browseIcon' => '',
                    'removeLabel' => '',
                    'removeIcon' => '',
                    'removeClass' => '',
                    'browseClass' => '',
                    'initialPreviewAsData' => true,
                    'overwriteInitial' => false,
                    'maxFileSize' => 5 * 1024 * 1024
                ]
            ]);
        }
    }

}