<?php


namespace backend\models\forms;


use borales\extensions\phoneInput\PhoneInputValidator;
use common\models\constants\CommonStatus;
use common\models\constants\UserRole;
use common\models\User;
use yii\base\Model;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string|null $full_name
 * @property string $phone
 * @property string $email
 * @property int $role
 * @property int $status
 * @property string $password
 * @property string $confirm_password
 * @property int $change_password
 *
 */
class UserRegisterForm extends Model
{
    public $id;
    public $full_name;
    public $phone;
    public $email;
    public $role;
    public $status;
    public $password;
    public $confirm_password;
    public $change_password;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['phone', 'full_name', 'password', 'confirm_password'], 'required'],
            [['role', 'status', 'change_password'], 'integer'],
            [['phone'], PhoneInputValidator::className()],
            ['status', 'default', 'value' => CommonStatus::STATUS_ACTIVE],
            ['status', 'in', 'range' => [CommonStatus::STATUS_NON_ACTIVE, CommonStatus::STATUS_ACTIVE, CommonStatus::STATUS_DELETED]],
            [['email', 'full_name', 'phone'], 'string', 'max' => 255],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => 'Parollar bir-biriga mos kelmaydi'],
            ['phone', 'unique', 'targetClass' => User::className(),
                'message' => 'Ushbu telefon nomer oldindan mavjud', 'when' => function ($model) {
                return !$model->id;
            }],
            ['email', 'unique', 'targetClass' => User::className(),
                'message' => 'Ushbu elektron pochta oldindan mavjud', 'when' => function ($model) {
                return !$model->id;
            }],
            ['role', 'default', 'value' => UserRole::ROLE_USER],
            ['role', 'in', 'range' => [UserRole::ROLE_USER, UserRole::ROLE_ACCOUNTANT, UserRole::ROLE_STAFF, UserRole::ROLE_RECTOR, UserRole::ROLE_ADMIN]]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Parol',
            'confirm_password' => 'Parolni tasdiqlang',
            'full_name' => 'To`liq ismi',
            'phone' => 'Telefon raqami',
            'email' => 'Elektron pochta',
            'status' => 'Status',
            'role' => 'Rol',
            'change_password' => 'Parolni o`zgartirish'
        ];
    }

    /**
     * @param User $user
     * @return UserRegisterForm
     */
    public static function getForm(User $user)
    {
        $form = new self();
        $form->id = $user->id;
        $form->phone = $user->phone;
        $form->email = $user->email;
        $form->full_name = $user->full_name;
        $form->role = $user->role;
        $form->status = $user->status;
        $form->password = $user->password_hash;
        $form->confirm_password = $user->password_hash;
        return $form;
    }

    /**
     * @return bool|User
     */
    public function register()
    {
        $user = new User();
        $user->creator_id = \Yii::$app->user->identity->id;
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->full_name = $this->full_name;
        $user->role = $this->role;
        if ($user->role == null)
            $user->role = UserRole::ROLE_USER;
        $user->status = $this->status;
        if ($user->status == null)
            $user->status = CommonStatus::STATUS_ACTIVE;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        if (!$user->save()) {
            return false;
        }
        return $user;
    }

    /**
     * @param User $user
     * @return bool|User
     */
    public function update(User $user)
    {
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->full_name = $this->full_name;
        $user->role = $this->role;
        $user->status = $this->status;
        if ($this->change_password != 0)
            $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        if (!$user->save()) {
            return false;
        }
        $this->id = $user->id;
        return $user;
    }
}