<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use common\models\constants\UserRole;

class BaseController extends Controller
{
    protected $role;

    /**
     * @param $action
     * @return bool|Response
     * @throws BadRequestHttpException
     */
    
    public function beforeAction($action)
    {
        if (($this->action->id !== 'login' && $this->action->id !== 'captcha' && Yii::$app->user->isGuest)) {
            Yii::$app->user->logout();
            $this->redirect(['/site/login']);
            return false;
        }
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->identity->seen;
            if (Yii::$app->user->identity->role == UserRole::ROLE_USER && !Yii::$app->user->identity->details->is_validate && ($this->action->id != 'update' || $this->action->id != 'upload') && $this->id != 'user-details') {
                $this->redirect(Url::to(['user-details/update', 'id' => Yii::$app->user->identity->detail_id]));
            }
        }
//        Yii::$app->session->setFlash('success', 'Unda ta\'qiqdan chiqarishga yordam bering');
//        Yii::$app->session->setFlash('warning', 'Please help me bro!');
        return parent::beforeAction($action);
    }

}