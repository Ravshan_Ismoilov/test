<?php

namespace backend\controllers;
 use common\models\Science;

class TestController extends BaseController
{
	// public $level;
	// public $science;

    
    public function actionIndex()
    {
        $model = new Science();
        return $this->render('index',['model' => $model]);
    }

    public function actionView()
    {
  	
    	$test = \common\models\Questions::find();
        return $this->render('view');
    }
}
