<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Log */

$this->title = $model->user->full_name. 'ning '.\common\helpers\Utilities::toStringDate($model->created_at).'dagi o`zgarishi';
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->full_name;
                }
            ],
            [
                'attribute' => 'text',
                'format' => 'raw'
            ],
            'created_at:datetime',
        ],
    ]) ?>

</div>
