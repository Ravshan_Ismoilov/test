<?php

use common\helpers\Utilities;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('yii', 'Tizimdagi o`zgarishlar vaqti');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'user_id',
                'value' => 'user.full_name',
                'label' => 'Foydalanuvchi',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'data' => ArrayHelper::map(\common\models\User::findActive()->all(), 'id', 'full_name'),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => Yii::t('yii', 'Tanlang...'),
                        'value' => $searchModel->user_id
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
            ],
            [
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function ($model) {
                    return substr(Html::label($model->text), 0, 100).'...';
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return Utilities::toStringDateTime($model->created_at);
                },
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attributeTo' => 'end',
                    'attribute' => 'begin',
                    'labelTo' => '-',
                    'value' => $searchModel->begin,
                    'valueTo' => $searchModel->end,
                    'clientOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'autoclose' => true
                    ]
                ]),
                'label' => 'O`zgarish bo`lgan sana'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
