<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
    $('.my').change(function() {
        if ($(this).val() != '')
            $(this).prev().text('Выбрано файлов: ' + $(this)[0].files.length);
        else
            $(this).closest('.box-form').children('.label').text('Exceldan yuklash');
});
</script>
<style type="text/css">
    .my {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
    .label {
        width: 180px;
        height: 50px;
        border-radius: 4px;
        text-align: center;
        cursor: pointer;
        display: block;
        font: 14px/50px Tahoma;
        transition: all 0.18s ease-in-out;
        border: 1px solid #333;
        color: #333;
    }
    
    .label:hover {
        color: white;
        background: #00ff00;
    }
</style>

<div class="container box-form">
    <h4>Exceldan yuklash (*.xlsx)</h4>
    <form action="<?= Url::to(['questions/import']) ?>" method="post">
        <div>
            <label for="myfile" class="label btn btn-info">Faylni tanlang</label>
        </div>
        <div>
            <input type="file" class="my" id="myfile" name="myfile">
        </div>
        <br>
        <button class="btn btn-success"> Yuklash </button>
    </form>
    <br>
<div>
<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'science')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'answer_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'answer_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'answer_three')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'answer_four')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
