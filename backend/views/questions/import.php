<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php
	if (isset($_FILES['file'])) {
		
		require_once __DIR__ . '/simplexlsx.class.php';
		
		if ( $xlsx = SimpleXLSX::parse( $_FILES['file']['tmp_name'] ) ) {

			echo '<h2>Parsing Result</h2>';
			echo '<table border="1" cellpadding="3" style="border-collapse: collapse">';

			list( $cols, ) = $xlsx->dimension();

			foreach ( $xlsx->rows() as $k => $r ) {
				//		if ($k == 0) continue; // skip first row
				echo '<tr>';
				for ( $i = 0; $i < $cols; $i ++ ) {
					echo '<td>' . ( ( isset( $r[ $i ] ) ) ? $r[ $i ] : '&nbsp;' ) . '</td>';
				}
				echo '</tr>';
			}
			echo '</table>';
		} else {
			echo SimpleXLSX::parse_error();
		}
	}
?>
</div>