<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="questions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            // 'creator_id',
            [
                'attribute' => 'creator_id',
                'value' => $model->creator->full_name,
            ],
            'science',
            'question',
            'answer_one',
            'answer_two',
            'answer_three',
            'answer_four',
            // 'status',
            [
                'attribute' => 'status',
                'value' => common\models\constants\CommonStatus::getString($model->status),
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'deleted_at:datetime',
        ],
    ]) ?>

</div>
