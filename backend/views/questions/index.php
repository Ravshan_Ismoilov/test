<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\constants\CommonStatus;
use common\models\constants\UserRole;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Questions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Questions'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'creator_id',
            [
                'attribute' => 'creator_id',
                'value' => function ($model) {
                    return $model->creator->full_name;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'creator_id',
                    'data' => ArrayHelper::map(\common\models\User::findActive()->all(), 'id', 'full_name'),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => Yii::t('yii', 'Tanlang...'),
                        'value' => $searchModel->creator_id
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
            ],
            // 'science',
            [
                'attribute' => 'science',
                'value' => function ($model) {
                    return $model->science;
                },
                // 'filter' => Select2::widget([
                //     'model' => $searchModel,
                //     'attribute' => 'creator_id',
                //     'data' => ArrayHelper::map(\common\models\Science::findActive()->all(), 'id', 'science_name'),
                //     'theme' => Select2::THEME_BOOTSTRAP,
                //     'options' => [
                //         'placeholder' => Yii::t('yii', 'Tanlang...'),
                //         'value' => $searchModel->creator_id
                //     ],
                //     'pluginOptions' => [
                //         'allowClear' => true
                //     ],
                // ]),
            ],
            'question',
            'answer_one',
            //'answer_two',
            //'answer_three',
            //'answer_four',
            // 'status',
            [
                'attribute' => 'status',
                'value' => function($model){
                    return CommonStatus::getString($model->status);
                },
                'filter' => CommonStatus::getArray()
            ],
            //'created_at',
            //'updated_at',
            //'deleted_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
