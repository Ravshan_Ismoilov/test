<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\models\forms\UserRegisterForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!$model->id) echo $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?php if ($model->id) echo $form->field($model, 'change_password')->checkbox(); ?>

    <?= $form->field($model, 'password')->textInput([
        'type' => 'password',
        'maxlength' => true,
        'autocomplete' => 'off'
    ]) ?>

    <?= $form->field($model, 'confirm_password')->textInput([
        'type' => 'password',
        'maxlength' => true,
        'autocomplete' => 'off'
    ]) ?>


    <?php if(!$model->id) echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if(Yii::$app->user->identity->role >= \common\models\constants\UserRole::ROLE_STAFF) echo $form->field($model, 'role')->dropDownList(\common\models\constants\UserRole::getArray()) ?>

    <?php if(Yii::$app->user->identity->role >= \common\models\constants\UserRole::ROLE_STAFF) echo $form->field($model, 'status')->dropDownList(\common\models\constants\CommonStatus::getArray()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
