<?php

use sultonov\cropper\CropperWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = "O`zgartirish";
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-photo">

    <?php $form = ActiveForm::begin(); ?>
    <h1><?=Html::encode('Rasmni o`zgartirish')?> </h1>

    <?php if($model->id) echo $form->field($model, 'image')->widget(CropperWidget::className(), [
        'uploadUrl' => Url::toRoute('/user/upload-photo'),
        'prefixUrl' => Yii::getAlias('@assets_url/user/avatar/'),
        'avatar' => true,
        'width' => 480,
        'height' => 480
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
