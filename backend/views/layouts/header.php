<?php

use common\helpers\Utilities;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">RI</span><span class="logo-lg"> Ravshan Ismoilov </span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= Yii::getAlias('@assets_url/user/avatar/') . Yii::$app->user->identity->image ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs">
                            <?= Yii::$app->user->identity->phone; ?>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= Yii::getAlias('@assets_url/user/avatar/') . Yii::$app->user->identity->image ?>" class="img-circle"
                                 alt="User Image"/>
                            <p>
                                <?= Yii::$app->user->identity->full_name; ?>
                                <small>Web Developer</small>
                            </p>
                        </li>
                        
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= Url::to(['user/photo', 'id' => Yii::$app->user->identity->id]) ?>" class="btn btn-default btn-flat">Rasmni o'zgartirish</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Chiqish',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
