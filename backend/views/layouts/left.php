<?php

use common\models\constants\UserRole;
use yii\helpers\Url;

$menu = [];
if (Yii::$app->user->identity->role >= UserRole::ROLE_USER)
    $menu[] = [
        'name' => 'Test',
        'icon' => 'fa fa-trash',
        'url' => ['test/index']
    ];
if (Yii::$app->user->identity->role >= UserRole::ROLE_RECTOR)
    $menu[] = [
        'name' => 'Savollar',
        'icon' => 'fa fa-question',
        'url' => ['questions/index']
    ];
if (Yii::$app->user->identity->role >= UserRole::ROLE_RECTOR)
    $menu[] = [
        'name' => 'Natija',
        'icon' => 'fa fa-check',
        'url' => ['result/index']
    ];
if (Yii::$app->user->identity->role >= UserRole::ROLE_RECTOR)
    $menu[] = [
        'name' => 'Fanlar',
        'icon' => 'fa fa-book',
        'url' => ['science/index']
    ];
if (Yii::$app->user->identity->role >= UserRole::ROLE_RECTOR)
    $menu[] = [
        'name' => 'Foydalanuvchi',
        'icon' => 'fa fa-user',
        'url' => ['user/index']
    ];
if (Yii::$app->user->identity->role >= UserRole::ROLE_RECTOR)
    $menu[] = [
        'name' => 'O\'zgarishlar',
        'icon' => 'fa fa-pencil',
        'url' => ['log/index']
    ];


?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::getAlias('@assets_url/user/avatar/') . Yii::$app->user->identity->image ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->full_name; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Меню</li>
            <?php foreach ($menu as $item): ?>
                <?php if (!isset($item['items'])): ?>
                    <li>
                        <a href="<?= Url::to($item['url']) ?>">
                            <i class="<?= $item['icon'] ?>"></i> <span><?= $item['name'] ?></span>
                        </a>
                    </li>
                <?php else: ?>
                    <li class="treeview" style="height: auto;">
                        <a href="#">
                            <i class="<?=$item['icon']?>"></i> <span><?=$item['name']?></span>
                            <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu" style="display: none;">
                            <?php foreach($item['items'] as $subitem): ?>
                            <li><a href="<?=Url::to($subitem['url'])?>"><i class="<?=$subitem['icon']?>"></i> <?=$subitem['name']?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>

    </section>

</aside>
