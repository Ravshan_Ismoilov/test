<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Science */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="science-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'science_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_of_questions')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\constants\CommonStatus::getArray()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
