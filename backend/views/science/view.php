<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\constants\UserRole;
use common\models\constants\CommonStatus;

/* @var $this yii\web\View */
/* @var $model common\models\Science */

$this->title = $model->science_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sciences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="science-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            // 'creator_id',
            [
                'attribute' => 'creator_id',
                'value' => $model->creator->full_name,
            ],
            'science_name',
            'number_of_questions',
            // 'status',
            [
                'attribute' => 'status',
                'value' => CommonStatus::getString($model->status),
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'deleted_at:datetime',
        ],
    ]) ?>

</div>
