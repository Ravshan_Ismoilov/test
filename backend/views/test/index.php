<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use common\models\Science;

/* @var $this yii\web\View */
/* @var $model common\models\Test */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Test');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
	<div class="row">
		<h1>Hurmatli <?= Yii::$app->user->identity->full_name ?></h1>
		

		<?= $form = ActiveForm::begin()

    		echo $form->field($model, 'science_name')
    		->dropdownList(Science::find()
    		->select(['science_name', 'id'])
    		->indexBy('id')
    		->column(),
    		['prompt'=>'Fanni tanlang']);

	    	echo $form->field($model, 'level')->dropDownList([
    			'1' => '1',
    			'2' => '2',
    			'3' => '3',
    			'4' => '4',
			]); ?>

			<p>
	    	    <?= Html::a(Yii::t('app', 'Boshlash'), ['view'], ['class' => 'btn btn-lg btn-danger']) ?>
		    </p>

	    <?= ActiveForm::end(); ?>

		<div class="alert text-justify" style="padding: 125px 100px 0px 10px;">
			<p>
		    	<code>BOSHLASH</code> tugmasi bosilgandan so'ng, sizga test savollari va mos ravishda test javoblari taqdim etiladi. Berilgan vaqt tugashi bilan sahifa avtomatik ravishda yopiladi. Javobsiz qolgan test savollari uchun qo'shimcha vaqt va ball berilmaydi. Shuning uchun barcha javoblarni belgilaganingizga ishonch hosil qilganingizdan so'ng <code>YAKUNLASH</code> tugmasiga bosing.
		    </p>
		</div>
	</div>
</div>
